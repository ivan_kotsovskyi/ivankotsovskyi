import styled from 'styled-components';

const StyledActions = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  @media print {
    display: none;
  }
`;

const StyledButton = styled.button`
  margin: 0 0.5rem;
  padding: 0;
  background-color: transparent;
  border: none;
  outline: none;
  cursor: pointer;

  svg {
    font-size: 1.5rem;
  }
`;

const StyledAnchor = styled.a`
  margin: 0 0.5rem;

  svg {
    color: black;
    font-size: 1.7rem;
  }
`;

export {
  StyledActions,
  StyledButton,
  StyledAnchor,
};
