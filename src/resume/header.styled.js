import styled from 'styled-components';

const StyledHeader = styled.header`
  display: flex;
  flex-direction: column;
  align-items: center;

  > * {
    margin: 0.5rem 0;
  }
`;

export {
  StyledHeader,
};
