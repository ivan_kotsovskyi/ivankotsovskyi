import React from 'react';
import styled from 'styled-components';

const Image = styled.img`
  width: 12.5rem;
  height: 12.5rem;
  border-radius: 1rem;

  @media print {
    display: none;
  }
`;

export default () => (
  <Image src="/images/photo2.jpg" alt="Ivan Kotsovskyi photo" />
);
