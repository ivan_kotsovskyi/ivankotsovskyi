import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faPrint } from '@fortawesome/free-solid-svg-icons';
import { StyledActions, StyledButton, StyledAnchor } from './actions.styled';

const print = () => window.print();

export default () => (
  <StyledActions>
    <StyledAnchor href="mailto:ivankotsovskyi@gmail.com" title="Send email" >
      <FontAwesomeIcon icon={faEnvelope} />
    </StyledAnchor>
    <StyledButton onClick={print} title="Print CV" >
      <FontAwesomeIcon icon={faPrint} />
    </StyledButton>
  </StyledActions>
);
