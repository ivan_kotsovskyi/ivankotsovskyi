import styled from 'styled-components';

const StyledSummary = styled.p`
  margin: 1rem 0;
`;

export {
  StyledSummary,
};
