export const companyToTable = (headings) => ({ name, data }) => ({
  name,
  table: headings.map((heading, index) => [heading, data[index]])
});

export const transformCompaniesData = ({ headings, data }) =>
  data.map(companyToTable(headings));
