import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const NameHeading = styled.h3`
  text-align: center;
`;

const StyledTableHead = styled.th`
  width: 12rem;
`;

const formTable = ([heading, data]) => (
  <tr key={data}>
    <StyledTableHead>{heading}</StyledTableHead>
    <td>{data}</td>
  </tr>
);

const InfoTable = ({ name, table }) => (
  <section>
    {name && <NameHeading>{name}</NameHeading>}
    <table>
      <tbody>
        {table.map(formTable)}
      </tbody>
    </table>
  </section>
);

InfoTable.propTypes = {
  name: PropTypes.string,
  table: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)).isRequired
};

export default InfoTable;
