import React, { Fragment } from 'react';
import useExperience from './useExperience';

import Header from './header';
import InfoTable from './info_table';
import Education from './education';
import { StyledSummary } from './index.styled';

const Experience = ({ companies }) => companies.map(
  ({ name, table }) => <InfoTable key={name} name={name} table={table} />
);

export default () => {
  const { qualification, companies } = useExperience();

  return (
    <Fragment>
      <Header />

      <StyledSummary>
        <b>Summary: </b>
        Results-driven software engineer with 5+ years of experience.
        Experienced in creating SPA applications using React.
        Passionate about programming languages and functional programming.
      </StyledSummary>

      <section>
        <h2>Highlights of Qualifications:</h2>
        <InfoTable
          name={qualification.name}
          table={qualification.table}
        />
      </section>

      <section>
        <h2>Professional Experience:</h2>
        <Experience companies={companies} />
      </section>

      <Education />

      <section>
        <h2>Languages:</h2>
        <p>
          Ukrainian (native), English (upper intermediate, TOEFL score: 91).
        </p>
      </section>
    </Fragment>
  );
};
