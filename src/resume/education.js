import React from 'react';

export default () => (
  <section>
    <h2>Education:</h2>
    <p>Ivan Franko National University of Lviv,
    Applied Mathematics and Informatics,
    Applied Mathematics, Bachelor's degree.</p>
    <p>Ivan Franko National University of Lviv,
    Applied Mathematics and Informatics,
    Applied Mathematics, Master's degree.</p>
  </section>
);
