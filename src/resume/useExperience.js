import data from './data';
import { transformCompaniesData } from './helpers';

const { qualification, companies } = data;

export default () => {
  return {
    qualification,
    rowCompanies: companies,
    companies: transformCompaniesData(companies),
  };
};
