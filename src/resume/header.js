import React from 'react';
import Photo from './photo';
import Actions from './actions';
import { StyledHeader } from './header.styled';

export default () => (
  <StyledHeader>
    <Photo />
    <h1>Ivan Kotsovskyi</h1>
    <h2>Software Engineer</h2>
    <Actions />
  </StyledHeader>
);
