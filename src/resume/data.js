const resumeData = {
  qualification: {
    table: [
      [
        'Programming Languages/ Technologies:',
        'JavaScript (TypeScript), React, HTML/CSS/SCSS, Redux, Jest, WebRTC, WebSockets'
      ],
      [
        'Familiar with:',
        'Erlang, Node.js, MongoDB, Terraform'
      ],
      [
        'Game Engines/Libraries:',
        'Phaser, PixiJS, three.js'
      ],
      [
        'Build Systems:',
        'Make, Webpack, Gulp.js'
      ],
      [
        'Operating Systems:',
        'Unix-like'
      ],
      [
        'Version Control:',
        'Git'
      ],
      [
        'Methodologies:',
        'SCRUM, Kanban'
      ],
      [
        'Others:',
        'Functional Programming, Design Patterns, OOP'
      ]
    ]
  },
  companies: {
    headings: [
      'Involvement Duration:',
      'Project Role:',
      'Project Team Size:',
      'Project description:'
    ],
    data: [
      {
        name: 'N-iX',
        data: [
          'Sept 2021 - Present',
          'Senior Software Engineer',
          '3 team members',
          'SCADA system'
        ]
      },
      {
        data: [
          'Mar 2020 - Sept 2021',
          'Senior Software Engineer',
          '3 team members',
          'FinTech project'
        ]
      },
      {
        name: 'Lohika',
        data: [
          'Jan 2019 - Mar 2020',
          'JavaScript Engineer',
          '3 team members',
          'Design system'
        ]
      },
      {
        name: 'Indeema Software',
        data: [
          'Dec 2017 - Dec 2018',
          'Front-end Developer',
          '5 team members',
          'Implemented SPA applications using React'
        ]
      },
      {
        name: 'Massive Heights',
        data: [
          'Apr 2016 — Dec 2017',
          'HTML5 Game Developer',
          '9 team members',
          'Implemented browser games and advertisement with JavaScript and HTML5'
        ]
      },
      {
        name: 'CharStudio',
        data: [
          'Mar 2015 – Jul 2015',
          'HTML5 Game Developer',
          '4 team members',
          'Implemented browser games with JavaScript and HTML5'
        ]
      }
    ]
  }
};

export default resumeData;
