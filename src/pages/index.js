import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";

import Resume from '../resume';

const description = `
Front-end Software Engineer with 5+ years of experience.
Experienced in creating SPA applications using React.
`;

const IndexPage = () => (
  <Layout>
    <SEO
      title="CV | Ivan Kotsovskyi"
      description={description}
      image="/images/photo2.jpg"
    />
    <Resume />
  </Layout>
);

export default IndexPage;
